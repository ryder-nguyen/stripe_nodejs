const functions = require('firebase-functions');
const admin = require("firebase-admin");
admin.initializeApp(functions.config().firebase);

const stripe = require("stripe")(functions.config().stripe.token);
exports.createStripeCustomer = functions.auth.user().onCreate(async (user) => {
  const customer = await stripe.customers.create({
    email: user.email,
    description: "My First Test Customer (created for API docs)",
  });

  console.log(customer.id);

  const stripeCustomersRef = admin
    .database()
    .ref(`stripe_customers/${user.uid}`);

  const customerId = customer.id;
  const userId = user.uid;

  console.log(customerId);
  console.log(userId);

  const customerData = {
    fullName: "Stripe User",
    customerId: customerId,
  };

  stripeCustomersRef.set(customerData);
});

//Create User Payment Intent
exports.createPaymentIntent = functions.https.onCall((data, context) => {
  console.log("creating payment intent");
  return stripe.paymentIntents.create({
    amount: data.amount,
    currency: data.currency,
    payment_method_types: ["card"]
  });
});

//Confirm Payment Intent
exports.confirmPaymentIntent = functions.https.onCall((data, context) => {
  console.log("confirm payment intent");
  var intentId = data.paymentIntentId;
  return stripe.paymentIntents.confirm(
    intentId,
    { payment_method: 'pm_card_visa' }
  );
});